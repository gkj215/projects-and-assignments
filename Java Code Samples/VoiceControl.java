package voicecontrol;
/*This is a slimmed down Speech to Text program, the only thing the program needs in a file location to store 
 the sound files. The program listens for a volume above a certain value, the begins to record what the user is 
 saying, and then sends to the google speech api.
 Your computer/mic may record with different sample rates, sample size (in bits) and channels, 
 and so they may need to be changed within the getAudioFormat() method in the SoundRecordingUtil class.

 Libraries needed: javaFlacEncoder http://sourceforge.net/projects/javaflacencoder/files/ 

 The user also needs an api key and access to the google speech api: http://www.chromium.org/developers/how-tos/api-keys

 Much of the ambient listening code came from this source, the user needs to add this as a source package folder.
 https://github.com/The-Shadow/java-speech-api

 */

import com.darkprograms.speech.microphone.MicrophoneAnalyzer;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaFlacEncoder.FLACFileWriter;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

public class VoiceControl {

    private final static MicrophoneAnalyzer microphone = new MicrophoneAnalyzer(FLACFileWriter.FLAC);

    // format of audio file
    AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;

    // the line from which audio data is captured
    TargetDataLine line;
    static String inputStr = "";

    private static final int RECORD_TIME = 6000;   // 60 seconds

    public VoiceControl() {

    }

    public static void main(String[] args) throws IOException {
        Scanner scan1 = new Scanner(System.in);
        System.out.println("This is the full Speech to Text Program, please enter a file path 'C:/Users/...' don't include a final '/'.");
        inputStr = scan1.nextLine();
        System.out.println();
        ambientListening();
    }

    public static void ambientListening() throws IOException {
        //File location hard coded in
        File wavFile = new File(inputStr + "/wavAudio.wav");

        try {
            microphone.captureAudioToFile(wavFile);
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        final int SILENT = microphone.getAudioVolume();
        boolean hasSpoken = false;
        boolean[] speaking = new boolean[10];
        Arrays.fill(speaking, false);
        for (int i = 0; i < 100; i++) {
            for (int x = speaking.length - 1; x > 1; x--) {
                speaking[x] = speaking[x - 1];
            }
            int frequency = microphone.getFrequency();
            int volume = microphone.getAudioVolume();
            speaking[0] = frequency < 255 && volume > SILENT && frequency > 85;
            System.out.println(speaking[0]);
            boolean totalValue = false;
            for (boolean bool : speaking) {
                totalValue = totalValue || bool;
            }
            //if(speaking[0] && speaking[2] && speaking[3] && microphone.getAudioVolume()>10){
            if (totalValue && microphone.getAudioVolume() > 20) {
                hasSpoken = true;
            }
            if (hasSpoken && !totalValue) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            }
        }
        if (hasSpoken) {
            microphone.close();
            voice();

        }
        ambientListening();
    }

    public static void voice() throws IOException {
        SoundRecordingUtil sruObj;
        sruObj = new SoundRecordingUtil(inputStr, inputStr);
        //end of else block
        //default filename for the wav file
        File wavFile = new File(inputStr + "/voiceRecord.wav");

        final SoundRecordingUtil recorder = new SoundRecordingUtil();

        // create a separate thread for recording
        Thread recordThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    recorder.start();
                } catch (LineUnavailableException ex) {
                    Logger.getLogger(VoiceControl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        recordThread.start();

        System.out.println("Recording...");
        try {
            Thread.sleep(RECORD_TIME);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        try {
            System.out.println("Stopping...");
            recorder.stop();
            recorder.save(wavFile);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        //convert from wav to flac
        sruObj.encode();

        //send to api
        sruObj.stt("");
    }
}