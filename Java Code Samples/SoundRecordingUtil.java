package voicecontrol;
/* This file contains much of the code for the program, all of the sound information is contained here.
 The methods for coverting and sending are here as well.

 The user api key goes here too, in the stt function.
 */

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javaFlacEncoder.FLAC_FileEncoder;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

public class SoundRecordingUtil {

    //Buffer Size needed for recording, and for the byte array
    private static final int BUFFER_SIZE = 4096;

    //Implements an output stream where the data is written to a byte array
    private ByteArrayOutputStream recordBytes;

    //Variable for reading audio, the TargetDataLine provides a method for reading the captured data from the buffer
    private TargetDataLine audioLine;

    //The AudioFormat is the class that specifies the arrangement of data in a sound stream, hence the getAudioFormat() method
    private AudioFormat format;

    private boolean isRunning;
    public String filePath = null;
    public String fileDest = null;

    public SoundRecordingUtil(String filePath) {
        this.filePath = filePath;
    }

    public SoundRecordingUtil(String filePath, String dest) {
        this.filePath = filePath;
        this.fileDest = dest;
    }

    public SoundRecordingUtil() {

    }

    //Defines a default audio format used to record, this needs to be changed based on the microphone being used    
    AudioFormat getAudioFormat() {
        float sampleRate = 44100;
        int sampleSizeInBits = 16;
        int channels = 2;
        boolean signed = true;
        boolean bigEndian = true;
        return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
    }

    /*Start recording sound     
     @throws LineUnavailableException if the system does not support the
     specified audio format or opens the audio data line.
     */
    public void start() throws LineUnavailableException {
        format = getAudioFormat();
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

        // checks if system supports the data line
//        if (!AudioSystem.isLineSupported(info)) {
//            throw new LineUnavailableException(
//                    "The system does not support the specified format.");
//        }
        audioLine = AudioSystem.getTargetDataLine(format);

        audioLine.open(format);
        audioLine.start();

        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = 0;

        recordBytes = new ByteArrayOutputStream();
        isRunning = true;

        while (isRunning) {
            bytesRead = audioLine.read(buffer, 0, buffer.length);
            recordBytes.write(buffer, 0, bytesRead);
        }
    }

    /*
     Stop recording sound, and save the file.     
     @param wavFile
     @throws IOException if any I/O error occurs.
     */
    public void stop() throws IOException {
        isRunning = false;

        if (audioLine != null) {
            audioLine.close();
        }

    }

    public void save(File wavFile) throws IOException {
        byte[] audioData = recordBytes.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(audioData);
        AudioInputStream audioInputStream = new AudioInputStream(bais, format,
                audioData.length / format.getFrameSize());

        AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, wavFile);

        audioInputStream.close();
        recordBytes.close();
    }

    public void encode() {
        //path of the flac file, same location from destination location in finish()
        FLAC_FileEncoder flacEncoder = new FLAC_FileEncoder();

        System.out.println();
        flacEncoder.encode(new File(this.filePath + "/voiceRecord.wav"), new File(this.filePath + "/flacAudio.flac"));
    }

    public void justEncode() {
        FLAC_FileEncoder flacEncoder = new FLAC_FileEncoder();

        System.out.println();
        flacEncoder.encode(new File(this.filePath), new File(this.fileDest + "/flacAudio.flac"));

        System.out.println("File converted.");
    }

    //the speech to text (stt) method sends the converted flac file to the google speech servers
    public void stt(String filePath) throws IOException {
        Path path;
        if (filePath.equals("")) {
            path = Paths.get(this.fileDest + "/flacAudio.flac");
        } else {
            path = Paths.get(filePath);
        }

        byte[] data = Files.readAllBytes(path);

        try {
            //link
            String request = "https://www.google.com/speech-api/v2/recognize?lang=en-us&key=[enter key here]";
            URL url = new URL(request);
            //open the connection
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            //set properties/timeout etc., the rate needs ot be changed to the rate value of the input microphone
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "audio/x-flac; rate=44100");
            connection.setRequestProperty("User-Agent", "speech2text");
            connection.setConnectTimeout(60000);
            connection.setUseCaches(false);

            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.write(data);
            wr.flush();
            wr.close();
            connection.disconnect();

            System.out.println("Result(s):");

            //get output
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String decodedString;
            decodedString = in.readLine();
            //exit if the result is null
            if ((decodedString = in.readLine()) == null) {
                System.out.println("No results found.");
                System.out.println();
                VoiceControl javaSTTObj = new VoiceControl();
                javaSTTObj.voice();
                //System.exit(0);
            } else {
                parse(decodedString);
            }

        } catch (IOException e) {

        } finally {

        }
    }

    //parse the result string into just the text output
    public void parse(String str) throws IOException {
        str = str.replace('"', ',');
        String strArray[];
        String tempStr = "";
        strArray = str.split("[{:,}]+");

        for (int i = 0; i < strArray.length; i++) {
            tempStr = strArray[i];
            if (tempStr.equals("transcript")) {
                tempStr = strArray[i + 1];
                System.out.println(tempStr);
                System.out.println();
                break;
            }
        }
        VoiceControl javaSTTObj = new VoiceControl();
        javaSTTObj.ambientListening();

    }
}